﻿using InTheHand.Net;
using InTheHand.Net.Bluetooth;
using InTheHand.Net.Sockets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            connectAsServer();
            if(serverStarted)
            {
                updateUI("Server already started");
                return;
            }
        }

        Guid mUUID = new Guid("00001101-0000-1000-8000-00805F9B34FB");
        bool serverStarted = false;
        private void connectAsServer()
        {
            serverStarted = true;
            updateUI("Servers tarted, waiting for connection");
            BluetoothListener blueListener = new BluetoothListener(mUUID);
            blueListener.Start();

            BluetoothClient conn = blueListener.AcceptBluetoothClient();
            updateUI("Client1 has connected");

            //BluetoothClient conn2 = blueListener.AcceptBluetoothClient();
            //updateUI("Client2 has connected");

            Stream mStream = conn.GetStream();

            //Stream mStream2 = conn2.GetStream();
            while (true)
            {
                byte[] received = new byte[1024];
                mStream.Read(received, 0, received.Length);
                updateUI("Received: " + Encoding.ASCII.GetString(received));
                //byte[] sent = Encoding.ASCII.GetBytes("Hello World");
                //mStream.Write(sent, 0, sent.Length);

                //byte[] received2 = new byte[1024];
                //mStream2.Read(received2, 0, received2.Length);
                //updateUI("Received: " + Encoding.ASCII.GetString(received2));
            }
        }

        private void updateUI(string message)
        {
            Func<int> del = delegate ()
            {
                Output.AppendText(message + System.Environment.NewLine);
                return 0;
            };
            Invoke(del);
        }
    }  
}
